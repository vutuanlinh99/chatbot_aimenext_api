from fastapi import FastAPI

from .service.predict import prediction

app = FastAPI()


@app.get("/result")
async def get_result_predict(sentence :str):
    list_proba, highest_proba = prediction(sentence)
    list_proba = prediction(sentence)
    return {"probability": list_proba, "predict":highest_proba}


# @app.get("/hello/{name}")
# async def say_hello(name: str):
#     return {"message": f"Hello {name}"}

import torch
from transformers import BertJapaneseTokenizer
from transformers import BertForSequenceClassification
map_label = {
0: '挨拶',
 1:'新規契約の流れ',
 2:'領収書発行',
 3:'ご注文のキャンセル',
 4:'お支払い方法',
 5:'配送',
 6:'パスワード変更',
 7:'サービス料',
 8:'解約手続き',
 9:' 返品・交換'
}

def prediction(sentence):

    model = BertForSequenceClassification.from_pretrained("cl-tohoku/bert-base-japanese-v2",
                                                          num_labels=10,
                                                          output_attentions=False,
                                                          output_hidden_states=False)
    #
    # device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # model.to(device)

    model.load_state_dict(torch.load('service/model/finetuned_BERT_epoch_20.model', map_location=torch.device('cpu')))
    tokenizer = BertJapaneseTokenizer.from_pretrained('cl-tohoku/bert-base-japanese')
    tok_test  = tokenizer(sentence,return_tensors="pt")
    with torch.no_grad():
          output = model(tok_test['input_ids'])
          return output[0].cpu().tolist(), map_label[int(output[0].cpu().numpy().argmax())]